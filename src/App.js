import React, { Fragment, useState } from 'react'
import { Footer, HeaderRow, TabGroup } from './controls/general'
import { Input, Chart } from './controls/tabs'

const App = ({ figures = [], setFigures, house = 0, setHouse }) => {
  let [selected, updateSelected] = useState(0)

  return (
    <Fragment>
      <HeaderRow title='Geomancy'
        onTabChange={updateSelected}
        tabValue={selected}
        tabs={[
          new HeaderRow.Tab('Input'),
          new HeaderRow.Tab('House Chart', figures.length === 0),
          new HeaderRow.Tab('Shield Chart', figures.length === 0)
        ]}
      />
      <TabGroup index={selected}>
        <Input setFigures={setFigures} />
        <Chart figures={figures} house={house} setHouse={setHouse} houseChart />
        <Chart figures={figures} house={house} setHouse={setHouse} shieldChart />
      </TabGroup>
      <Footer />
    </Fragment>
  )
}

export default App
