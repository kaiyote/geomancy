import PropTypes from 'prop-types'
import React from 'react'
import { Tab as T, Tabs } from '.'
import logo from '../../logo.svg'
import './HeaderRow.css'

class Tab {
  constructor (label, disabled = false) {
    this.label = label
    this.disabled = disabled
  }
}

const HeaderRow = ({ title = null, onTabChange, tabValue, tabs, hasLogo = true, small = false }) => (
  <header className={`HeaderRow ${(small ? 'small border' : '')}`}>
    {hasLogo ? <img src={logo} className='HeaderRow-logo' alt='logo' /> : null}
    {title ? <p>{title}</p> : null}
    <Tabs onChange={onTabChange} value={tabValue} small={small}>
      {tabs.map((t, i) => <T label={t.label} disabled={t.disabled} key={i} />)}
    </Tabs>
  </header>
)

HeaderRow.propTypes = {
  onTabChange: PropTypes.func.isRequired,
  tabValue: PropTypes.number.isRequired,
  tabs: PropTypes.arrayOf(PropTypes.instanceOf(Tab))
}

HeaderRow.Tab = Tab

export default HeaderRow
