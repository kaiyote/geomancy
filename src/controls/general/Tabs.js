import React, { Children, cloneElement } from 'react'
import PropTypes from 'prop-types'
import './Tabs.css'

const Tabs = ({ children, onChange, value, small = false }) => (
  <div className={`Tabs ${(small ? 'small' : null)}`}>
    {Children.map(children, (c, i) => cloneElement(c, {
      selected: i === value,
      onClick: onChange,
      index: i,
      key: i
    }))}
  </div>
)

const Tab = ({ label, disabled = false, selected = false, onClick = _ => undefined, index = 0 }) => (
  <div className={`Tab ${(selected ? 'selected' : '')} ${(disabled ? 'disabled' : '')}`}
    onClick={() => disabled ? null : onClick(index)}
  >
    {label}
  </div>
)

const TabGroup = ({ children, index }) => (
  <React.Fragment>
    {children.map((child, idx) => index === idx && child)}
  </React.Fragment>
)

Tabs.propTypes = {
  children: PropTypes.arrayOf(PropTypes.element).isRequired,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.number.isRequired
}

Tab.propTypes = {
  label: PropTypes.string.isRequired,
  disabled: PropTypes.bool,
  selected: PropTypes.bool,
  onClick: PropTypes.func,
  index: PropTypes.number
}

export { Tab, TabGroup, Tabs }
