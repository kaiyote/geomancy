import React, { Fragment, useState } from 'react'
import { HeaderRow, TabGroup } from '../general'
import { Generate, Manual } from './inputs'

const Input = ({ setFigures }) => {
  let [selected, updateSelected] = useState(0)

  return (
    <Fragment>
      <HeaderRow hasLogo={false} small
        tabValue={selected} onTabChange={updateSelected}
        tabs={[
          new HeaderRow.Tab('Manual'),
          new HeaderRow.Tab('Generate')
        ]}
      />
      <TabGroup index={selected}>
        <Manual setFigures={setFigures} />
        <Generate setFigures={setFigures} />
      </TabGroup>
    </Fragment>
  )
}

export default Input
