import React from 'react'

const parseFormat = (formatString, data) =>
  formatString
    .split(/\r?\n/)
    .map((line, index) => (
      <p key={index}>{line.trim().replace(/\[(.+?)]/g, (_, p1) => data[p1])}</p>
    ))

const Info = ({ children, data }) =>
  data
    ? <div className='Info'>
      {parseFormat(children, data)}
    </div>
    : <div className='Info' />

export default Info
