import React from 'react'
import { Figures } from '../../../../data'
import './Figure.css'

const numberLabel = num => {
  switch (num) {
    case 1:
      return '1st'
    case 2:
      return '2nd'
    case 3:
      return '3rd'
    default:
      return num + 'th'
  }
}

const __onHover = (figure, label, callback, index) => {
  let data = Figures.find(e => e.value === figure)

  data.label = `${index ? numberLabel(index) + ' ' : ''}${label.replace(/^(.)(.+)$/, (_, p1, p2) => `${p1.toUpperCase()}${p2}`)}`

  callback && callback(null, data)
}

const FigureRow = ({ val, way = false }) => (
  <div>
    <span className={`FigureRow ${(way ? 'way' : '')}`}>•</span>
    {val === 0 ? <span className={`FigureRow ${(way ? 'way' : '')}`}>•</span> : null}
  </div>
)

const Figure = ({ figure, label, onHover, index, className, style = {}, quesited = false, querent = false, ofTheWay = false, projection = false }) => (
  <div className={`${className} ${(quesited ? 'quesited' : '')} ${(querent ? 'querent' : '')} ${(projection ? 'projection' : '')}`} style={style}
    onMouseOver={onHover && (() => __onHover(figure, label, (_, d) => onHover(d), index))}
    title={[querent ? 'Querent' : null, quesited ? 'Quesited' : null, projection ? 'Index' : null].filter(x => x !== null).join('/')}>
    <FigureRow val={(figure >> 3) & 1} way={ofTheWay} />
    <FigureRow val={(figure >> 2) & 1} />
    <FigureRow val={(figure >> 1) & 1} />
    <FigureRow val={figure & 1} />
  </div>
)

export default Figure
