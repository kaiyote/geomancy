import React, { useState } from 'react'
import './HouseChart.css'
import { Figure, Info } from './components'
import util from '../../../util'

const HouseChart = ({ figures, house = 1 }) => {
  let [info, setInfo] = useState(null)
  let _figures = new Array(...figures)
  let projection = util.projectionOfPoints(_figures)

  return (
    <div className='HouseChart'>
      <svg className='Background' viewBox='0 0 4 4'>
        <g>
          <rect x='0' y='0' width='4' height='4' />
          <rect x='1' y='1' width='2' height='2' />
          <polyline points='0,0 1,1 2,0 3,1 4,0' />
          <polyline points='0,4 1,3 2,4 3,3 4,4' />
          <polyline points='1,1 0,2 1,3' />
          <polyline points='3,1 4,2 3,3' />
        </g>
      </svg>
      {_figures.splice(0, 4).map((m, i) => (
        <Figure
          figure={m}
          className={`Figure Mother${i}`}
          key={i}
          index={i + 1}
          label='mother'
          onHover={data => setInfo(data)}
          quesited={i + 1 === house}
          querent={i === 0}
          projection={i + 1 === projection}
        />
      ))}
      {_figures.splice(0, 4).map((d, i) => (
        <Figure
          figure={d}
          className={`Figure Daughter${i}`}
          key={i}
          index={i + 1}
          label='daughter'
          onHover={data => setInfo(data)}
          quesited={i + 5 === house}
          projection={i + 5 === projection}
        />
      ))}
      {_figures.splice(0, 4).map((n, i) => (
        <Figure
          figure={n}
          className={`Figure Niece${i}`}
          key={i}
          index={i + 1}
          label='niece'
          onHover={data => setInfo(data)}
          quesited={i + 9 === house}
          projection={i + 9 === projection}
        />
      ))}
      <Info data={info}>
        {`[name] ([translation])
        [meaning]`}
      </Info>
    </div>
  )
}

export default HouseChart
