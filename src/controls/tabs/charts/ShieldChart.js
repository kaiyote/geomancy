import React, { Fragment, useState } from 'react'
import './ShieldChart.css'
import { Figure, Info } from './components'
import util from '../../../util'

const ToggleLine = ({ x1, y1, x2, y2, show }) =>
  <line x1={x1} y1={y1} x2={x2} y2={y2} visibility={show ? 'visible' : 'hidden'} />

const inWayOfPoints = (wayOfPoints, index) => wayOfPoints.findIndex(x => x === index) > -1

const ShieldChart = ({ figures, house = 1 }) => {
  let [info, setInfo] = useState(null)
  let _figures = new Array(...figures)
  let wayOfPoints = util.wayOfPoints(_figures)
  let projection = util.projectionOfPoints(_figures)

  return (
    <Fragment>
      <div className='ShieldChart'>
        <svg className='Background' viewBox='0 0 8 4'>
          <g>
            <polygon points='0,0 8,0 8,2 6,4 2,4 0,2' />
            <line x1='0' y1='1' x2='8' y2='1' />
            <line x1='0' y1='2' x2='8' y2='2' />
            <line x1='1' y1='0' x2='1' y2='1' />
            <line x1='2' y1='0' x2='2' y2='2' />
            <line x1='3' y1='0' x2='3' y2='1' />
            <line x1='4' y1='0' x2='4' y2='3' />
            <line x1='5' y1='0' x2='5' y2='1' />
            <line x1='6' y1='0' x2='6' y2='2' />
            <line x1='7' y1='0' x2='7' y2='1' />
            <line x1='1' y1='3' x2='7' y2='3' />
            <rect x='7' y='3' width='1' height='1' />
          </g>
        </svg>
        <svg className='Way' viewBox='0 0 32 16' >
          <g className='thick'>
            <ToggleLine x1='16' y1='13' x2='8' y2='11.15' show={inWayOfPoints(wayOfPoints, 13)} />
            <ToggleLine x1='16' y1='13' x2='24' y2='11.15' show={inWayOfPoints(wayOfPoints, 12)} />
            <ToggleLine x1='8' y1='9' x2='4' y2='7.15' show={inWayOfPoints(wayOfPoints, 11)} />
            <ToggleLine x1='8' y1='9' x2='12' y2='7.15' show={inWayOfPoints(wayOfPoints, 10)} />
            <ToggleLine x1='24' y1='9' x2='20' y2='7.15' show={inWayOfPoints(wayOfPoints, 9)} />
            <ToggleLine x1='24' y1='9' x2='28' y2='7.15' show={inWayOfPoints(wayOfPoints, 8)} />
            <ToggleLine x1='4' y1='5' x2='2' y2='3.15' show={inWayOfPoints(wayOfPoints, 7)} />
            <ToggleLine x1='4' y1='5' x2='6' y2='3.15' show={inWayOfPoints(wayOfPoints, 6)} />
            <ToggleLine x1='12' y1='5' x2='10' y2='3.15' show={inWayOfPoints(wayOfPoints, 5)} />
            <ToggleLine x1='12' y1='5' x2='14' y2='3.15' show={inWayOfPoints(wayOfPoints, 4)} />
            <ToggleLine x1='20' y1='5' x2='18' y2='3.15' show={inWayOfPoints(wayOfPoints, 3)} />
            <ToggleLine x1='20' y1='5' x2='22' y2='3.15' show={inWayOfPoints(wayOfPoints, 2)} />
            <ToggleLine x1='28' y1='5' x2='26' y2='3.15' show={inWayOfPoints(wayOfPoints, 1)} />
            <ToggleLine x1='28' y1='5' x2='30' y2='3.15' show={inWayOfPoints(wayOfPoints, 0)} />
          </g>
        </svg>
        {_figures.splice(0, 4).map((m, i) => (
          <Figure
            figure={m}
            className='Figure'
            key={i}
            index={i + 1}
            label='mother'
            onHover={data => setInfo(data)}
            style={{ gridArea: `m${i}` }}
            querent={i === 0}
            quesited={i + 1 === house}
            ofTheWay={wayOfPoints.indexOf(i) > -1}
            projection={i + 1 === projection}
          />
        ))}
        {_figures.splice(0, 4).map((m, i) => (
          <Figure
            figure={m}
            className='Figure'
            key={i}
            index={i + 1}
            label='daughter'
            onHover={data => setInfo(data)}
            style={{ gridArea: `d${i}` }}
            quesited={i + 5 === house}
            ofTheWay={wayOfPoints.indexOf(i + 4) > -1}
            projection={i + 5 === projection}
          />
        ))}
        {_figures.splice(0, 4).map((m, i) => (
          <Figure
            figure={m}
            className='Figure'
            key={i}
            index={i + 1}
            label='niece'
            onHover={data => setInfo(data)}
            style={{ gridArea: `n${i}` }}
            quesited={i + 9 === house}
            ofTheWay={wayOfPoints.indexOf(i + 8) > -1}
            projection={i + 9 === projection}
          />
        ))}
        {_figures.splice(0, 2).map((m, i) => (
          <Figure
            figure={m}
            className='Figure'
            key={i}
            index={i + 1}
            label='witness'
            onHover={data => setInfo(data)}
            style={{ gridArea: `w${i}` }}
            ofTheWay={wayOfPoints.indexOf(i + 12) > -1}
          />
        ))}
        {_figures.map((m, i) => (
          <Figure
            figure={m}
            className='Figure'
            key={i}
            index={i + 1}
            label={['judge', 'reconciler'][i]}
            onHover={data => setInfo(data)}
            style={{ gridArea: `f${i}` }}
            ofTheWay={wayOfPoints.length > 1 && i === 0}
          />
        ))}
      </div>
      <Info data={info}>
        {`[name] ([translation])
        [meaning]`}
      </Info>
    </Fragment>
  )
}

export default ShieldChart
