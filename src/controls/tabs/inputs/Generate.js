import React, { useState } from 'react'
import { fillState } from './Manual'
import './Generate.css'

let timer = null
const timeoutMax = 1000

const genDot = (dots, setDots, currentRow, setCurrentRow, setFigures) => {
  dots[currentRow]++
  setDots(dots)
  if (timer == null) {
    timer = setTimeout(() => {
      timer = null
      setCurrentRow(++currentRow)
      if (currentRow > 15) setFigures(fillState(new Array(...dots)))
    }, Math.random() * timeoutMax)
  }
}

const Generate = ({ setFigures }) => {
  let [dots, setDots] = useState(new Array(16).fill(0))
  let [currentRow, setCurrentRow] = useState(0)
  const disabled = currentRow > 15

  return (
    <div className='Generate'>
      <div className={`Button ${(disabled ? 'disabled' : '')}`}
        onClick={() => disabled ? null : genDot(dots, setDots, currentRow, setCurrentRow, setFigures)}>
        <span>Click to make a dot</span>
      </div>
      <div className='Dots'>
        {dots.map((d, i) => <p key={i}>
          {new Array(d).fill(null).map(x => '•').join('')}
        </p>)}
      </div>
    </div>
  )
}

export default Generate
