import React, { useState } from 'react'
import util from '../../../util'
import './Manual.css'

const numbersToFigure = (fire, air, water, earth) =>
  (fire % 2 << 3) | (air % 2 << 2) | (water % 2 << 1) | (earth % 2)

export const fillState = (bits, e) => {
  e && e.preventDefault()
  let myBits = bits
  if (bits.filter(x => typeof x === 'number').length !== 16) return
  const mother1 = numbersToFigure(...myBits.splice(0, 4))
  const mother2 = numbersToFigure(...myBits.splice(0, 4))
  const mother3 = numbersToFigure(...myBits.splice(0, 4))
  const mother4 = numbersToFigure(...myBits)
  const result = util.generate([mother1, mother2, mother3, mother4])
  return result
}

const Manual = ({ setFigures }) => {
  let [bits, setBits] = useState(new Array(16))

  return (
    <form onSubmit={e => setFigures(fillState(bits, e))}>
      <div className='Manual'>
        {
          ['One', 'Two', 'Three', 'Four'].map((x, i) =>
            <div key={i} style={{ gridArea: `M${x}` }}><span>Mother {x}</span></div>)
        }
        {
          ['Fire', 'Air', 'Water', 'Earth'].map((x, i) =>
            <div key={i} style={{ gridArea: `E${x}` }}><span>{x}</span></div>)
        }
        {
          ['One', 'Two', 'Three', 'Four'].map((y, i) =>
            ['Fire', 'Air', 'Water', 'Earth'].map((x, i2) =>
              <div key={`${i}.${i2}`} style={{ gridArea: `${x}${y}` }}>
                <input type='number' min={0} max={50} onChange={e => {
                  bits[i * 4 + i2] = +e.target.value
                  setBits(bits)
                }} />
              </div>))
        }
        <button type='submit' style={{ gridArea: 's' }}>Generate</button>
      </div>
    </form>
  )
}

export default Manual
