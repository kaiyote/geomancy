import React, { Fragment } from 'react'
import { firstBy } from 'thenby'
import { Houses } from '../../data'
import './Chart.css'
import { HouseChart, ShieldChart } from './charts'

const TopicList = () =>
  <Fragment>
    {[].concat(...Houses.map(h => h.topics.map(t => ({ value: h.number, label: t }))))
      .sort(firstBy(x => x.value).thenBy('label'))
      .map(x => (
        <option value={`${x.value}${x.label}`} key={x.label}>
          {x.label}
        </option>
      ))}
  </Fragment>

const Chart = ({ figures, house, setHouse, houseChart = false, shieldChart = false }) => (
  <div className='Chart'>
    <div className='selector'>
      <label htmlFor='House'>Select Topic: </label>
      <select name='House' value={house} onChange={e => setHouse(e.target.value)}>
        <TopicList />
      </select>
    </div>
    {houseChart ? <HouseChart figures={figures} house={houseLabelToInt(house)} /> : null}
    {shieldChart ? <ShieldChart figures={figures} house={houseLabelToInt(house)} /> : null}
  </div>
)

const houseLabelToInt = house => +house.match(/^\d+/)[0]

export default Chart
