import React, { useState } from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import './index.css'

const rootEl = document.getElementById('root')

const Root = ({ Component }) => {
  let [figures, setFigures] = useState([])
  let [house, setHouse] = useState('1Beginnings')

  return <Component figures={figures} setFigures={setFigures} house={house} setHouse={setHouse} />
}

ReactDOM.render(<Root Component={App} />, rootEl)

// @ts-ignore
if (module.hot) {
  // @ts-ignore
  module.hot.accept('./App', () => {
    const NextApp = require('./App').default
    ReactDOM.render(<Root Component={NextApp} />, rootEl)
  })
}
