const elements = ['earth', 'water', 'air', 'fire']

const elementToBit = element => elements.indexOf(element.toLowerCase())

const recombine = (figList, element) => {
  const index = elementToBit(element)

  return (getBit(index, figList[0]) << 3) | (getBit(index, figList[1]) << 2) |
    (getBit(index, figList[2]) << 1) | getBit(index, figList[3])
}

const add = figures => {
  const results = []
  for (let i = 0; i < figures.length - 1; i++) results.push(figures[i] ^ figures[++i])
  return results
}

const getBit = (position, figure) => (figure >> position) & 1

const generate = mothers => {
  if (mothers.length !== 4) {
    throw new Error('There must be exactly 4 mothers')
  }
  if (mothers.find(x => typeof x !== 'number' || x < 0 || x > 15)) {
    throw new Error('Every mother must be a number between 0 and 15 inclusive')
  }

  const daughters = elements.map(e => recombine(mothers, e)).reverse()
  const nieces = add(mothers.concat(daughters))
  const witnesses = add(nieces)
  const judge = add(witnesses)[0]
  const reconciler = add([judge, mothers[0]])[0]

  return mothers.concat(daughters).concat(nieces).concat(witnesses).concat([judge, reconciler])
}

const getParents = index => {
  switch (index) {
    case 14: return [13, 12]
    case 13: return [11, 10]
    case 12: return [9, 8]
    case 11: return [7, 6]
    case 10: return [5, 4]
    case 9: return [3, 2]
    case 8: return [1, 0]
    default: return []
  }
}

const wayOfPoints = (figures, index = 14, useActive = null) => {
  const current = figures[index]
  const active = !!getBit(3, current)
  let result = []
  if (useActive === null || active === useActive) {
    result.push(index)
    for (const par of getParents(index)) result = result.concat(wayOfPoints(figures, par, active))
  }
  return result
}

const projectionOfPoints = figures => {
  return [].concat(...figures.filter((_, i) => i < 12)
    .map(fig => [0, 1, 2, 3].map(y => getBit(y, fig)))
  ).filter(x => x === 1).length % 12 || 12
}

export default { generate, projectionOfPoints, wayOfPoints }
