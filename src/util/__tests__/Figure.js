import util from '..'

describe('generate', () => {
  it('throws an error if the wrong number of mothers are supplied', () => {
    expect(() => util.generate([])).toThrow('There must be exactly 4 mothers')
    expect(() => util.generate([1, 2, 3])).toThrow('There must be exactly 4 mothers')
    expect(() => util.generate([1, 2, 3, 4, 5])).toThrow('There must be exactly 4 mothers')
    expect(() => util.generate(['cat', 1, 2, 3])).toThrow('Every mother must be a number between 0 and 15 inclusive')
    expect(() => util.generate([-3, 1, 2, 3])).toThrow('Every mother must be a number between 0 and 15 inclusive')
    expect(() => util.generate([1, 2, 20, 3])).toThrow('Every mother must be a number between 0 and 15 inclusive')
  })

  it('generates an array containing the mothers', () => {
    const result = util.generate([0, 2, 5, 15])
    expect(result).toHaveLength(16)
    expect(result).toEqual(expect.arrayContaining([0, 2, 5, 15]))
  })

  it('generates the correct collection of figures', () => {
    const result = util.generate([0, 2, 5, 15])
    expect(result).toEqual([0, 2, 5, 15, 1, 3, 5, 3, 2, 10, 2, 6, 8, 4, 12, 12])
  })
})

describe('wayOfPoints', () => {
  it.each`
    figures | expected
    ${[0, 2, 5, 15, 1, 3, 5, 3, 2, 10, 2, 6, 8, 4, 12, 12]} | ${[14, 12, 9, 3]}
    ${[13, 0, 12, 6, 10, 11, 1, 8, 13, 10, 1, 9, 7, 8, 15, 2]} | ${[14, 13, 11, 7]}
  `('correctly computes the way of points', ({ figures, expected }) => {
  const result = util.wayOfPoints(figures)
  expect(result).toEqual(expected)
})
})

describe('projection of points', () => {
  it('should project to house 12 when all figures are populus', () => {
    const result = util.projectionOfPoints([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1])
    expect(result).toEqual(12)
  })

  it('should project correctly', () => {
    const result = util.projectionOfPoints([0, 2, 5, 15, 1, 3, 5, 3, 2, 10, 2, 6, 8, 4, 12, 12])
    expect(result).toEqual(8)
  })
})
